#include "Vehicle.h"

Model Vehicle::vehicleModel;

Vehicle::Vehicle()
{
	speed = 0.0f;
	targetSpeed = 60.0f;
	posX = 0.0f;
	posY = 0.0f;
	startX = 0.0f;
	acc = 0.0f;
	change = 1;
	ACC_speed = 100.0f;
	ACC_distance = 40.0f;
}

void Vehicle::loadModel()
{
	vehicleModel.create("data/Car/BMW_M3_GTR.obj");
}

void Vehicle::Render(Shader shader)
{
	glm::mat4 model2;
	//model2 = glm::rotate(model2, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	model2 = glm::translate(model2, glm::vec3(posX + startX, -1.75f, posY));
	model2 = glm::scale(model2, glm::vec3(0.001f, 0.001f, 0.001f));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
//	cout << spd << std::endl;
	vehicleModel.Draw(shader);
}

float Vehicle::getPositionX()
{
	return startX + posX;
}

float Vehicle::getPositionY()
{
	return posY;
}

void Vehicle::change_lane_right(Shader shader, float t)
{
	if (posY >= 12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, 12.0f*t));
	posY = posY + 12.0f*t;
	if (posY <= 12.5f && posY >= 11.5f) posY = 12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::set_position(float x, float y)
{
	startX = x;
	posY = y;
}

void Vehicle::change_lane_left(Shader shader, float t)
{
	if (posY <= -12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, -12.0f*t));
	posY = posY - 12.0f*t;
	if (posY >= -12.5f && posY <= -11.5f) posY = -12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::change_lane_center(Shader shader, float t)
{
	if (posY < 0.0f) change_lane_right(shader, t);
	else if (posY > 0.0f) change_lane_left(shader, t);
	if (posY < 0.5f && posY > -0.5f) posY = 0.0f;
}

void Vehicle::ACC_System(vector<Vehicle*> traffic, float time)
{
	static vector<float> distances;
	static float vehicleDistance = -1, vehiclePosX = -1;
	static int vehicleIndex = -1, auxIndex = 0;
	bool change = false;
	for (int index = 1; index < traffic.size(); index++)
	{
		if (abs(this->getPositionY() - traffic[index]->getPositionY()) < 12.0f && this->getPositionX() < traffic[index]->getPositionX())
		{
			if (traffic[index]->getPositionX() - this->getPositionX() < 60 && vehiclePosX == -1)
			{
				change = true;
				vehiclePosX = traffic[index]->getPositionX();
				if (vehicleIndex != index)
				{
					vehicleIndex = index;
				}
			}
			else if (traffic[index]->getPositionX() - this->getPositionX() < 60 && vehiclePosX != -1)
			{
				vehiclePosX = traffic[index]->getPositionX();
				change = true;
				if (vehicleIndex != index)
				{
					vehicleIndex = index;
				}
			}
		}
		else if (index == traffic.size() - 1 && change == false)
		{
			vehicleIndex = -1;
			vehiclePosX = -1;
			distances.clear();
		}
	}
		if (vehicleIndex == -1)
		{
			this->set_targetSpeed(ACC_speed);
		}
		else
		{
			if (vehicleIndex != auxIndex)
			{
				distances.clear();
				auxIndex = vehicleIndex;
			}
			distances.push_back(traffic[vehicleIndex]->getPositionX() - this->getPositionX());
			if (distances.size() == 2)
			{
				float vehicleSpeed = (distances[1] - distances[0]) / time + this->getSpeed();
				if (vehicleSpeed > ACC_speed)
					this->set_targetSpeed(ACC_speed);
				else if (this->getSpeed() - vehicleSpeed > 30.0f)
					this->set_targetSpeed(vehicleSpeed);
				else if (distances[1] < ACC_distance)
				{
					if (abs(vehicleSpeed - this->getSpeed()) >= 30.0f) this->set_targetSpeed(vehicleSpeed - 5.0f, 3);
					else if (abs(vehicleSpeed - this->getSpeed()) >= 20.0f && abs(vehicleSpeed - this->getSpeed()) < 30.0f) this->set_targetSpeed(vehicleSpeed - 5.0f, 2);
					else if (abs(vehicleSpeed - this->getSpeed()) >= 10.0f && abs(vehicleSpeed - this->getSpeed()) < 20.0f) this->set_targetSpeed(vehicleSpeed - 5.0f);
					else this->set_targetSpeed(0.0f);
				}
				else if (distances[1] > ACC_distance && distances[1] < ACC_distance + 10.0f)
					this->set_targetSpeed(vehicleSpeed);
				else this->set_targetSpeed(ACC_speed);
				distances.clear();
			}
		}
	cout << this->getSpeed() << " ; " << this->getTargetSpeed() << endl;
}

//TODO acceleratia sa se modifice in functie de diverse cazuri
void Vehicle::accelerate(float t)
{
/*	if (acceleratePriority == 0)
		acc = acc + 20.0f*t;
	if(acceleratePriority == 1)
		acc = acc  + 5.0f*t;
	if (acceleratePriority == 2)
		acc = acc + 10.0f*t;
	if (acceleratePriority == 3)
		acc = acc + 2.0f*t;*/
	acc = accMAX * (1 - speed / speedMAX);
	speed = speed + acc * t;
	if (speed >= targetSpeed)
	{
		speed = targetSpeed;
		//acc = 0.0f;
		controlAcc = true;
		acceleratePriority = 0;
	}
}

//TODO acceleratia sa se modifice in functie de diverse cazuri
void Vehicle::brake(float t)
{
	brk = bMAX * (1 + (speed) / speedMAX);
	speed = speed - brk*t;
	if (speed <= targetSpeed)
	{
		speed = targetSpeed;
		//acc = 0.0f;
		controlAcc = true;
		brakePriority = 0;
	}
}

void Vehicle::set_targetSpeed(float tSpeed, int priority)
{
/*	if (speed != targetSpeed && controlAcc)
	{
		acc = 0.0f;
		controlAcc = false;
	}
	if (tSpeed > speed)
		acceleratePriority = priority;
	else brakePriority = priority;*/
	targetSpeed = tSpeed;
}

float Vehicle::getSpeed()
{
	return speed;
}

float Vehicle::getTargetSpeed()
{
	return targetSpeed;
}

void Vehicle::motion(float t)
{
	if (targetSpeed > speed) accelerate(t);
	if (targetSpeed < speed) brake(t);
	posX = posX + speed * t + (acc * pow(t,2) / 2);
}

void Vehicle::set_speed(float spd)
{
	speed = spd;
}

void Vehicle::set_ACC_speed(float spd)
{
	ACC_speed = spd;
}

void Vehicle::set_ACC_dist(float dst)
{
	ACC_distance = dst;
}
