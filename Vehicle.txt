#pragma once
#include "Model.h"
#include "Header.h"
#include <GLFW/glfw3.h>

class Vehicle
{
public:
	Vehicle();	//constructor

	void Render(Shader shader);
	void loadModel();

	//schimbari de lane
	void change_lane_right(Shader shader, float t);
	void change_lane_left(Shader shader, float t);
	void change_lane_center(Shader shader, float t);

	//ACC System
	void ACC_System(vector<Vehicle*>,float);

	//functii pt deplasare
	void accelerate(float t);
	void brake(float t);
	void motion(float t);

	//setters
	void set_targetSpeed(float tSpeed, int priority = 0);
	void set_position(float x, float y);
	void set_speed(float spd);
	void set_ACC_speed(float spd);
	void set_ACC_dist(float dst);

	//getters
	float getPositionX();
	float getPositionY();
	float getSpeed();
	float getTargetSpeed();

	static Model vehicleModel;
	int change = 0, usrCMD = 0;
	float chng;
	bool usrChLane = false;
	bool chLane = false;
	
private:
	float speed, targetSpeed;
	float posX, posY;
	float startX;
	float acc;
	float brk = 0.0f;
	float ACC_speed, ACC_distance;
	int brakePriority = 0;
	int acceleratePriority = 0;
	const float speedMAX = 180.0f, accMAX = 30.0f, bMAX = 30.0f;
	bool controlAcc = true;
};
