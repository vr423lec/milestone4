#include "Simulator.h"
#include <stdlib.h>
#include <time.h>

Simulator::Simulator(GLFWwindow* win) : window(win), gfx(win)
{
	gfx.Init();
	shader.create("data\\Shaders\\model_loading.vert", "data\\Shaders\\model_loading.frag");
	ourVehicle = new Vehicle();
	ourVehicle->loadModel();
	ourRoad.setup(shader);
	ourVehicle->set_position(-45.0f, 0.0f);
	Vehicle* veh1, *veh2;
	veh1 = new Vehicle();
	veh2 = new Vehicle();
	veh2->set_position(55.0f, 12.0f);
	veh1->set_position(45.0f, 0.0f);
	traffic.push_back(ourVehicle);
	traffic.push_back(veh1);
	traffic.push_back(veh2);
	srand(std::time(NULL));
	screenText.setup();
}

Simulator::~Simulator()
{
	delete ourVehicle;
	for (int i = 1; i < traffic.size(); i++) delete traffic[i];
} 

void Simulator::Go(float deltatime, int FPS)
{
	gfx.BeginFrame();
	CreateFrame();

	manageTraffic();	//verifica si modifica continutul vectorului trafic

	ourVehicle->ACC_System(traffic, deltatime);
	ourVehicle->motion(deltatime);	// deplasarea vehiculului
//	cout << ourVehicle->getTargetSpeed() << " ; " << ourVehicle->getSpeed() << endl;

	//schimbare viteza trafic in mod random
	if (randomVar > 8000 && randomVar < 8100)
	{
		trSpeed = rand() % (traffic.size() - 1) + 1;
		bool chSpeed = true; 
		for (int i = 1; i < traffic.size(); i++)
		{
			if ((traffic[trSpeed]->getPositionY() == traffic[i]->getPositionY() && traffic[trSpeed]->getPositionX() < traffic[i]->getPositionX()))
				chSpeed = false;
		}
		if (chSpeed == true) traffic[trSpeed]->set_targetSpeed(rand() % 80 + 60.0f);
	}

	traffic_control(deltatime);		//controleaza deplasarea traficului

	//resetare variabile in urma efectuarii schimbari de lane
	if (ourVehicle->getPositionY() == 12.0f || ourVehicle->getPositionY() == 0.0f || ourVehicle->getPositionY() == -12.0f)
	{
		ourVehicle->change = 0;
		test = false;
		int index = rand() % 10000 + 1;
		randVar = ourVehicle->getPositionY();
		randomVar = index;
	}
	menu();		//afisare meniu
	manage_menu();		//control meniu
	//FPS rendering
	string fps;
	stringstream ss;
	ss << FPS;
	fps = "FPS:" + ss.str();
	screenText.render_text(fps.c_str(), 540.0f, 570.0f, 0.5f, glm::vec3(0.3, 0.7f, 0.9f));
	gfx.EndFrame();
}

void Simulator::CreateFrame()
{
	shader.Use();
	//setare camera
	glm::mat4 projection = glm::perspective(glm::radians(90.0f), (float)800.0f / (float)600.0f, 0.1f, 100.0f);
	glm::mat4 view = glm::lookAt(glm::vec3(ourVehicle->getPositionX(), 40.0f, 0.0f), glm::vec3(ourVehicle->getPositionX(), 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	//desenare vehicul principal
	ourVehicle->Render(shader);
	//desenare drum
	ourRoad.Render(shader, ourVehicle->getPositionX());
	//desenare trafic
	for (int i = 1; i < traffic.size(); i++)
	{
		traffic[i]->Render(shader);
	}
}

void Simulator::traffic_control(float deltatime)
{
	//se parcurg toate elementele din vectorul de trafic, mai putin primul care 
	//este reprezentat de vehiculul principal

	for (int i = 1; i < traffic.size(); i++)
	{
		//verificam daca user-ul a introdus o comanda de schimbare de directie
		//pt vehiculul respectiv
		if (traffic[i]->usrChLane == false)
		{
			//daca nu a introdus, parcurgem din nou vectorul
			for (int pos = 0; pos < traffic.size();pos++)
			{
				//daca viteza vehiculului pe care il verificam este mai mare decat
				//cea a vehiculului de referinta si se afla pe aceeasi banda
				//la o distanta mica una de cealalta, se va schimba lane-ul
				if (traffic[i]->getPositionY() == traffic[pos]->getPositionY() && (traffic[pos]->getPositionX() - traffic[i]->getPositionX() > 0) && traffic[pos]->getPositionX() - traffic[i]->getPositionX() < 30.0f && (traffic[i]->getTargetSpeed() > traffic[pos]->getTargetSpeed() || traffic[i]->getSpeed() > traffic[pos]->getSpeed()))
					traffic[i]->chLane = true;
				if(traffic[i]->chLane == true)
				{
					if (traffic[i]->chng == -12.0f)
					{
						float distC = checkDistanceC(*traffic[i]);
						float distL = checkDistanceL(*traffic[i]);
						if (abs(distC - distL) < 30.0f && distC != -1 && distL != -1) traffic[i]->set_targetSpeed(60.0f,3);
						else if (distC > 30.0f || distC == -1) traffic[i]->change_lane_center(shader, deltatime/2);
					}
					if (traffic[i]->chng == 12.0f)
					{
						float distC = checkDistanceC(*traffic[i]);
						float distR = checkDistanceR(*traffic[i]);
						if (abs(distC - distR) < 30.0f && distC != -1 && distR != -1) traffic[i]->set_targetSpeed(60.0f,3);
						else if (distC > 30.0f || distC == -1) traffic[i]->change_lane_center(shader, deltatime/2);
					}
					else if (traffic[i]->chng == 0.0f)
					{
						float distR = checkDistanceR(*traffic[i]);
						float distL = checkDistanceL(*traffic[i]);
						if (abs(distR - distL) < 30.0f && distR != -1 && distL != -1) traffic[i]->set_targetSpeed(60.0f,3);
						if (distR == -1 && traffic[i]->change == 0)
							traffic[i]->change = 1;
						else if (distL == -1 && traffic[i]->change == 0)
							traffic[i]->change = 2;
						else if (distR > distL && distR > 30.0f && traffic[i]->change == 0) traffic[i]->change = 3;
						else if (distR < distL && distL > 30.0f &&traffic[i]->change == 0) traffic[i]->change = 4;
						if (traffic[i]->change == 1) traffic[i]->change_lane_right(shader, deltatime/2);
						else if (traffic[i]->change == 2) traffic[i]->change_lane_left(shader, deltatime/2);
						else if (traffic[i]->change == 3) traffic[i]->change_lane_right(shader, deltatime/2);
						else if (traffic[i]->change == 4) traffic[i]->change_lane_left(shader, deltatime/2);
					}
				}
			}
		}
		//daca utilizatorul a introdus o comanda de schimbare lane
		else
		{
			//verificam comanda si schimbam pe banda corespunzatoare
			if (traffic[i]->usrCMD == 1 && checkIfSafeL(*traffic[i]) == true) traffic[i]->change_lane_left(shader, deltatime);
			if (traffic[i]->usrCMD == 2 && checkIfSafeC(*traffic[i]) == true) traffic[i]->change_lane_center(shader, deltatime);
			if (traffic[i]->usrCMD == 3 && checkIfSafeR(*traffic[i]) == true) traffic[i]->change_lane_right(shader, deltatime);
		}
		//resetam starea variabilelor daca vehiculul este centrat pe banda
		if (traffic[i]->getPositionY() == 12.0f || traffic[i]->getPositionY() == 0.0f || traffic[i]->getPositionY() == -12.0f)
		{
			traffic[i]->chLane = false;
			traffic[i]->usrChLane = false;
			traffic[i]->chng = traffic[i]->getPositionY();
			traffic[i]->change = 0;
		}
		traffic[i]->motion(deltatime);	//deplasarea vehiculului
	}
}


void Simulator::manageTraffic()
{
	//verificam toate vehiculele mai putin pe cel principal, camera fiind centrata
	//pe acesta
	for (int i = 1;i < traffic.size();i++)
	{
		float poses[] = { -12.0f,0.0f,12.0f };
		float y = poses[rand() % 3];
		//verificam daca vehiculul a iesit din spatiul camerei prin fata
		if (traffic[i]->getPositionX() > ourVehicle->getPositionX() + 100.0f)
		{
			//stergem vehiculul
			delete traffic[i];
			traffic.erase(traffic.begin() + i);
			//cream un vehicul nou
			Vehicle* veh = new Vehicle();
			//setam pozitia de start
			veh->set_position(ourVehicle->getPositionX() - (rand() % 30) - 70.0f, y);
			//setam viteza
			veh->set_speed(ourVehicle->getTargetSpeed() + 20.0f);
			//adaugam la trafic
			traffic.push_back(veh);
		}
		if (traffic[i]->getPositionX() < ourVehicle->getPositionX() - 100.0f)
		{
			delete traffic[i];
			traffic.erase(traffic.begin() + i);
			Vehicle* veh = new Vehicle();
			veh->set_position(ourVehicle->getPositionX()+ (rand()%30) + 70.0f, y);
			veh->set_speed(ourVehicle->getTargetSpeed() - 20.0f);
			traffic.push_back(veh);
		}
	}
}

void Simulator::menu()
{
	if (currentMenuState == 0)
	{
		screenText.render_text("1 - Set ACC Speed&Distance", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		screenText.render_text("2 - Add new vehicle", 10.0f, 520.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		screenText.render_text("3 - Set new action", 10.0f, 500.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if (currentMenuState == 1)
	{
		if(subMenuState == 0)
			screenText.render_text("Set new speed:", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		else screenText.render_text("Set new distance:", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if (currentMenuState == 2)
	{
		if (subMenuState == 0)	screenText.render_text("Enter vehicle speed:", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		else if (subMenuState == 1) screenText.render_text("Enter vehicle target speed:", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		else screenText.render_text("Please select the vehicle's starting position", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if (currentMenuState == 3)
	{
		if (subMenuState == 0) screenText.render_text("Select vehicle", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
		else 
		{
			screenText.render_text("1 - Accelerate", 10.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			screenText.render_text("2 - Slow down", 10.0f, 520.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			screenText.render_text("3 - Switch to right lane", 10.0f, 500.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			screenText.render_text("4 - Switch to center lane", 10.0f, 480.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			screenText.render_text("5 - Switch to left lane", 10.0f, 460.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));

		}
	}
}

void Simulator::manage_menu()
{
	if (currentMenuState == 0)
	{
		counter++;
		if (counter > 5)
		{
			if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) currentMenuState = 1;
			if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) currentMenuState = 2;
			if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) currentMenuState = 3;
			counter = 0;
		}
	}
	if (currentMenuState == 1)
	{
		if (subMenuState == 0)
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || number == "")
			{
				counter++;
				if (counter > 10)
				{
					int inputNum = checkForInput();
					if (inputNum == 10 && number != "")
					{
						number.pop_back();
					}
					if (inputNum != -1 && inputNum!=10)
					{
						stringstream num;
						num << inputNum;
						number = number + num.str();
					}
					counter = 0;
				}
				screenText.render_text(number.c_str(), 145.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			}
			else
			{
				ourVehicle->set_ACC_speed(atof(number.c_str()));
				number = "";
				subMenuState = 1;
			}
		}
		else
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || number == "")
			{
				counter++;
				if (counter > 10)
				{
					int inputNum = checkForInput();
					if (inputNum == 10 && number != "")
					{
						number.pop_back();
					}
					if (inputNum != -1 && inputNum!=10)
					{
						stringstream num;
						num << inputNum;
						number = number + num.str();
					}
					counter = 0;
				}
				screenText.render_text(number.c_str(), 165.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			}
			else
			{
				ourVehicle->set_ACC_dist(atof(number.c_str()));
				currentMenuState = 0;
				subMenuState = 0;
				number = "";
			}
		}
	}

	if (currentMenuState == 2)
	{
		if (checkADD == false)
		{
			newVeh = new Vehicle();
			checkADD = true;
		}
		if (subMenuState == 0)
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || number == "")
			{
				counter++;
				if (counter > 10)
				{
					int inputNum = checkForInput();
					if (inputNum == 10 && number != "")
					{
						number.pop_back();
					}
					if (inputNum != -1 && inputNum!=10)
					{
						stringstream num;
						num << inputNum;
						number = number + num.str();
					}
					counter = 0;
				}
				screenText.render_text(number.c_str(), 200.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			}
			else
			{
				newVeh->set_speed(atoi(number.c_str()));
				subMenuState = 1;
				counter = 0;
				number = "";
			}
		}
		else if (subMenuState == 1)
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || number=="")
			{
				counter++;
				if (counter > 10)
				{
					int inputNum = checkForInput();
					if (inputNum == 10 && number!="")
					{
						number.pop_back();
					}
					if (inputNum != -1 && inputNum!=10)
					{
						stringstream num;
						num << inputNum;
						number = number + num.str();
					}
					counter = 0;
				}
				screenText.render_text(number.c_str(), 240.0f, 540.0f, 0.4f, glm::vec3(1.0f, 0.0f, 0.0f));
			}
				else
				{
					newVeh->set_targetSpeed(atoi(number.c_str()));
					subMenuState = 2;
					counter = 0;
					number = "";
				}
		}
		else
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || cursorX == 0)
			{
				gfx.draw_cursor(ourVehicle->getPositionX() + cursorX, cursorY, shader);
				if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) cursorY = -1.0f;
				if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) cursorY = 0.0f;
				if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) cursorY = 1.0f;
				if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) cursorX++;
				if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) cursorX--;
			}
			else
			{
				newVeh->set_position(ourVehicle->getPositionX() + cursorX, cursorY * 12);
				cursorX = 0;
				cursorY = 0;
				subMenuState = 0;
				currentMenuState = 0;
				traffic.push_back(newVeh);
				checkADD = false;
			}
		}
	}
	if (currentMenuState == 3)
	{
		if (subMenuState == 0)
		{
			if (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS || cursorX == 0)
			{
				counter++;
				gfx.draw_cursor(traffic[cursorX]->getPositionX(), traffic[cursorX]->getPositionY()/12, shader);
				if (counter > 10)
				{
					if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
					{
						if (cursorX > 0)
							cursorX--;
					}
					if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
					{
						if (cursorX < traffic.size() - 1)
							cursorX++;
					}
					counter = 0;
				}
			}
			else
			{
				subMenuState = 1;
				cursorY = 0;
				counter = 0;
			}
		}
		else
		{
			if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
			{
				traffic[cursorX]->set_targetSpeed(traffic[cursorX]->getTargetSpeed() + 50.0f);
				cursorX = 0;
				subMenuState = 0;
				currentMenuState = 0;
			}
			if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
			{
				traffic[cursorX]->set_targetSpeed(traffic[cursorX]->getTargetSpeed() - 50.0f);
				cursorX = 0;
				subMenuState = 0;
				currentMenuState = 0;
			}
			if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
			{
				traffic[cursorX]->usrChLane = true;
				traffic[cursorX]->usrCMD = 3;
				cursorX = 0;
				subMenuState = 0;
				currentMenuState = 0;
			}
			if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
			{
				traffic[cursorX]->usrChLane = true;
				traffic[cursorX]->usrCMD = 2;
				cursorX = 0;
				subMenuState = 0;
				currentMenuState = 0;
			}
			if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
			{
				traffic[cursorX]->usrChLane = true;
				traffic[cursorX]->usrCMD = 1;
				cursorX = 0;
				subMenuState = 0;
				currentMenuState = 0;
			}
		}
	}
}


int Simulator::checkForInput()
{
	if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)	return 0;
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)	return 1;
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)	return 2;
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)	return 3;
	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)	return 4;
	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)	return 5;
	if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)	return 6;
	if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS)	return 7;
	if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS)	return 8;
	if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS)	return 9;
	if (glfwGetKey(window, GLFW_KEY_BACKSPACE) == GLFW_PRESS)	return 10;
	return -1;
}

float Simulator::checkDistanceR(Vehicle veh)
{
	static float distance = 1234;
	for (int i = 0; i < traffic.size(); i++)
	{
		if (traffic[i]->getPositionX() == veh.getPositionX() && traffic[i]->getPositionY() == veh.getPositionY())
			continue;
		if (traffic[i]->getPositionY() == 12.0f)
		{
			float newDist = abs(veh.getPositionX() - traffic[i]->getPositionX());
			if (distance > newDist) distance = newDist;
		}
	}
	if(distance == 1234)
		return -1;
	else
	{
		float x = distance;
		distance = 1234;
		return x;
	}
}

float Simulator::checkDistanceL(Vehicle veh)
{
	static float distance = 1234;
	for (int i = 0; i < traffic.size(); i++)
	{
		if (traffic[i]->getPositionX() == veh.getPositionX() && traffic[i]->getPositionY() == veh.getPositionY())
			continue;
		if (traffic[i]->getPositionY() == -12.0f)
		{
			float newDist = abs(veh.getPositionX() - traffic[i]->getPositionX());
			if (distance > newDist) distance = newDist;
		}
	}
	if (distance == 1234)
		return -1;
	else
	{
		float x = distance;
		distance = 1234;
		return x;
	}
}

float Simulator::checkDistanceC(Vehicle veh)
{
	static float distance = 1234;
	for (int i = 0; i < traffic.size(); i++)
	{
		if (traffic[i]->getPositionX() == veh.getPositionX() && traffic[i]->getPositionY() == veh.getPositionY())
			continue;
		if (traffic[i]->getPositionY() == 0.0f)
		{
			float newDist = abs(veh.getPositionX() - traffic[i]->getPositionX());
			if (distance > newDist) distance = newDist;
		}
	}
	if (distance == 1234)
		return -1;
	else
	{
		float x = distance;
		distance = 1234;
		return x;
	}
}

bool Simulator::checkIfSafeC(Vehicle trVeh)
{
	for (int pos = 0; pos < traffic.size(); pos++)
		if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && traffic[pos]->getPositionY() == 0.0f)
			return false;
	return true;
}

bool Simulator::checkIfSafeR(Vehicle trVeh)
{
	for (int pos = 0; pos < traffic.size(); pos++)
		if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && (traffic[pos]->getPositionY() == 12.0f || trVeh.getPositionY() + 12.0f == traffic[pos]->getPositionY()))
			return false;
	return true;
}

bool Simulator::checkIfSafeL(Vehicle trVeh)
{
	for (int pos = 0; pos < traffic.size(); pos++)
		if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && (traffic[pos]->getPositionY() == -12.0f || trVeh.getPositionY() - 12.0f == traffic[pos]->getPositionY()))
			return false;
	return true;
}